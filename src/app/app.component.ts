import { Component } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-animated-product-card';

  url: string = "https://yome.vn/cach-rua-anh-tu-dien-thoai/imager_138088.jpg";
    imageChange(event: any){
        this.url = event.target.src;
    }

  partners: any = [
    {
      imgUrl: 'https://ephoto360.com/uploads/effect-data/ephoto360.com/i66ln8t31/1t-min60b5fcb209e63.jpg'
    },
    {
      imgUrl: 'https://ephoto360.com/uploads/worigin/2019/07/10/wolf-min5d25815f20a9d_d7d1b9d51469b84cb1388824bd8a1ee6.jpg'
    },
    {
      imgUrl: 'https://i.pinimg.com/originals/c1/cc/28/c1cc28e820d9e646a4919dcaaa64f6cc.jpg'
    },
    {
      imgUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTRXQkxAjqMYGC50Y9Oyb2mArcBvzME8KCDgA&usqp=CAU'
    },
    {
      imgUrl: 'https://ephoto360.com/uploads/effect-data/ephoto360.com/32fc3a3f5/wolf2min5ecdea117f803.jpg'
    },
    {
      imgUrl: 'https://ephoto360.com/uploads/effect-data/ephoto360.com/32fc3a3f5/55ea15bbab5641.jpg'
    },
  ];

  partnerOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 6
      }
    },
    nav: false
  }

  policyOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    navSpeed: 700,
    navText: ['<i class="fa fa-caret-left"></i>', '<i class="fa fa-caret-right"></i>'],
    nav: true,
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 3
      }
    },
  }
}
